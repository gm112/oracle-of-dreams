//Taken from: https://github.com/webpack/webpack-with-common-libs/blob/master/Gruntfile.js
//TODO: Refactor to ES6 syntax

var webpackConfig = require("./webpack.config.js");

module.exports = function(grunt) {
	//require("matchdep").filterAll("grunt-*").forEach(grunt.loadNpmTasks);
	grunt.loadNpmTasks('grunt-webpack');
	//grunt.loadNpmTasks('grunt-babel');
	var webpack = require("webpack");

	grunt.initConfig({
		//game
		"webpack": {
			options: webpackConfig,
			build: {
				plugins: webpackConfig.plugins.concat(
					new webpack.DefinePlugin({
						"process.env": {
							// This has effect on the react lib size
							"NODE_ENV": JSON.stringify("production")
						}
					}),
					new webpack.optimize.DedupePlugin()//,
					//new webpack.optimize.UglifyJsPlugin()
				)
			},
			"build-dev": {
				devtool: "sourcemap",
				debug: true
			}
		},
		"webpack-dev-server": {
			options: {
				webpack: webpackConfig,
				publicPath: "/" + webpackConfig.output.publicPath
			},
			start: {
				keepAlive: true,
				webpack: {
					devtool: "eval",
					debug: true
				}
			}
		},

		watch: {
			game: {
				files: ["engine/engine/src/**","game/src/**", "static/**"],
				tasks: ["webpack:build-dev"],
				options: {
					spawn: false,
				}
			}
		},

	});
	// The development server (the recommended option for development)
	grunt.registerTask("default", ["webpack-dev-server:start"]);

	// Build and watch cycle (another option for development)
	// Advantage: No server required, can run app from filesystem
	// Disadvantage: Requests are not blocked until bundle is available,
	//               can serve an old app on too fast refresh
	grunt.registerTask("dev", ["webpack:build-dev"]);

	// Production build
	grunt.registerTask("build", ["webpack:build"]);
};

'use strict';
import DefaultState from 'blue-gps-engine/src/states/game_state';
import WebSocketEntity from '../entities/internal/net/WebSocketEntity';
/*
So what we're going to do here is...

The game is going to show splash screens during loading,
and one of which will be ZFGC's logo as this is meant to
pay homage to a community that has gone to influence many
lives beyond the troubles it has had over the years.

After that, we'll load the web socket stuff, which will
expose the WebRTC layer. The Entity object will have to be
refactored to look for a special case where networking is enabled.
Upon setting up its properties, it'll also enable subscribing
fields to network-related events.

The update loop will continually stream updates as it updates each Entity.
State updates coming from the client, where upon arrival will call the entity involved and
will trigger the network event attached. Keeping logic client-echo-server-pong is to excuse
the client from having ownership over the the gloval world state. As the client will respect
what the server tells it, above what's in memory. The server will be open to recieving updates
at any time, and will relay the packets to all clients and server's who are involved with the state,
and then will queue the state update for processing when its update loop fires. States can be
flagged to be excluded from the queueing mechanism for UDP or similar paths where the protocol
is lossy.


Side-Note: On the server side of things, servers will essentially be in charge of its own little
set of things. The protocol will support a method of flagging state updates to be passed around to other
server's that are related. (i.e. global chat messages) Or some instances will be solely responsible for one special
function. This is to keep the load from being distributed into one funnel and instead, directly to the multiple
destinations. Though from an inside perspective, a server may be load-balanced via a p2p model to help handle heavy load.
(though this last bit is probably not going to be attempted as I wouldn't need to handle that kind of use case.. no to load balancing)

 Thankfully, we can model this in a sane fashion using WebSockets and NodeJs. We can tie in events to act as the sending and recieving triggers,
 and when a flag on the Entity specifies a networking behaviour, the engine and server will handle it through cooperation, as the server is the
 one who tells the story to the client.

*/
class GameState extends DefaultState {
	constructor(game, object) {
		super(game, object);
		this.name = 'network_game_state';
		this.socket = null;

		this.players = {};
		this.pending_frames = [];
		this.message_listeners = {};
		this.net = {
			driver: new WebSocketEntity(this),
			socket: null,
			config: this.engine.net || {}
		};
	}

	init() {
		super.init(...arguments);
		//TODO: Init web driver.
	}

	create() {
		this.net.driver.create();
		super.create(...arguments);

	}

	//Listener = {type: '', emit: () => {} };
	registerMessageListener(listener) {
		if(!listener || !listener.emit || typeof(listener.emit) !== 'function'
		|| !listener.namespace || this.message_listeners[listener.namespace]) return;

		this.message_listeners[listener.namespace] = listener;
	}


	registerFrame(entity) {
		if(!entity || !entity.getCurrentState || typeof(entity.getCurrentState) !== 'function') return;
		this.pending_frames.push(entity.getCurrentState());
	}

	update() {
		super.update(...arguments);
		this.onProcessFrames();
	}

	//onResumedCallback :

	//onStateChange It is dispatched only when the new state is started, which isn't usually at the same time as StateManager.start
	onProcessFrames() {
		this.pending_frames.forEach((frame) => { this.net.driver.send(frame); });
	}

	handlePlayerUpdates(players) {
		players.forEach((player_frame) => {
			let player = this.players[player_frame.id];

			if(!player) {
				//delete players[player_frame];
				return;
			}

			this.net.socket.send(JSON.stringify({type: 'set_state', namespace: 'state.players', id: player_frame.id, frame: player_frame}));

		});
	}

	handleCustomMessage(message) {

	}

}

export default GameState;

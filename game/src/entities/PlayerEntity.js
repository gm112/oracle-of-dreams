'use strict';

import Entity from './NetworkEntity';
import phaser from 'phaser';

class PlayerEntity extends Entity {
	constructor(game, object) {
		super(...arguments);
		this.map = null;
		this.layers = null;
		this.actions = this.engine.actions;
	}

	create() {
		super.create(...arguments);
		this.game.camera.follow(this.sprite);

		this.map = this.engine.world.map;
		this.layers = this.engine.world.layers;
		this.engine.player = this;

		this.sprite.animations.add('walkUp', [0, 1, 2], 3.5, true);
		this.sprite.animations.add('walkRight', [3, 4, 5], 3.5, true);
		this.sprite.animations.add('walkDown', [6, 7, 8], 3.5, true);
		this.sprite.animations.add('walkLeft', [9, 10, 11], 3.5, true);

		this.sprite.smoothed = false;
		this.sprite.scale.set(3);
		this.sprite.body.fixedRotation = true;

		this.state.registerMessageListener({namespace: 'state.players', emit: (message) => {
				//Something.
				switch(message.type) {
					case 'server_update_state':
						this.handleStateUpdate(message.state);
					break;
					default:
					console.warn('Unabled message detected.');
					console.log(message);
					break;
				}
			}
		});

		this.connect();
	}

	//TODO: Add logic to handle multiple local players
	connect() {
		this.state.net.driver.send({type: 'client_connect', namespace: 'state.players', players: [this.getCurrentState()] });
	}

	//This Entity is going to have tile editing functionality.
	update() {
		super.update(...arguments);
		this.handleMovement();
		this.state.pending_frames.push({type: 'client_set_state', namespace: 'state.players', frame: this.getCurrentState()});
	}

	handleMovement() {
		let inputIsDown = false;

		if(this.actions.walk_up.isDown()) {
			//this.sprite.body.thrust(300);
			if(!inputIsDown) this.sprite.animations.play('walkUp');
			inputIsDown = true;
		}

		if(this.actions.walk_down.isDown()) {
			//this.sprite.body.reverse(300);
			if(!inputIsDown) this.sprite.animations.play('walkDown');
			inputIsDown = true;
		}

		if(this.actions.walk_left.isDown()) {
			//this.sprite.body.thrustLeft(300);
			if(!inputIsDown) this.sprite.animations.play('walkLeft');
			inputIsDown = true;
		}

		if(this.actions.walk_right.isDown()) {
		//	this.sprite.body.thrustRight(300);
			if(!inputIsDown) this.sprite.animations.play('walkRight');
			inputIsDown = true;
		}

		if(!inputIsDown) {
			this.sprite.animations.stop();
		//	this.sprite.body.setZeroVelocity();
			//this.sprite.callAll('animations.stop', 'animations');
		}

	}

	getCurrentState() {
		return {
			x: this.sprite.body.x,
			y: this.sprite.body.y,
			name: this.name,
			type: this.type,
			actions: this.actions
		};

	}


}

export default PlayerEntity;

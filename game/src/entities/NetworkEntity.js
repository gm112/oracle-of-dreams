'use strict';
import Entity from 'blue-gps-engine/src/entities/Entity';

class NetworkEntity extends Entity {
  constructor() {
    super(...arguments);
  }

  handleStateUpdate(entity_state) {
    for(let key in entity_state)
      this[key] = entity_state[key];
  }

  getCurrentState() {
    return {
      x: this.x,
      y: this.y,
      name: this.name,
      type: this.type,
      actions: this.actions
    };

  }


}

export default NetworkEntity;

'use strict';

class WebSocketEntity {
  constructor(state) {
    if(!state) throw new Exception('Error: Empty state object passed to WebSocketEntity constructor.');
    this.state = state;
  }

  create() {
    this.state.net.uri = `${this.state.engine.config.net.prefix}://${this.state.engine.config.net.host}${this.state.engine.config.net.port ? ':' + this.state.engine.config.net.port : ''}`; //new Uri(`${this.state.engine.config.net.prefix}://${this.state.engine.config.net.host}${this.state.engine.config.port ? ':' + this.state.engine.config.port : ''}`);
    this.state.net.socket = new WebSocket(this.state.net.uri);
    this.socket = this.state.net.socket;
    this.socket.onconnect = () => { this.onConnect(...arguments); };
    this.socket.onmessage = () => { this.onMessage(...arguments); };
    this.socket.onerror = () => { this.onError(...arguments); };
    this.socket.onclose = () => { this.onClose(...arguments); };
    let killWhile = false,
    counter = 0;

    while(this.socket.readyState === 0 || !killWhile)
    {
      counter++;
      console.log('Waiting on socket to be ready...');

      if(counter > 1000) killWhile = true;
    }
  }

  send(message) {
    if(typeof(message) === 'string') return this.socket.send(message);

    let temp_buffer = this.objectToBuffer(message),
      buffer = new Uint8Array(temp_buffer);

    this.socket.send(buffer);
  }

  objectToBuffer(object) {
    if(!object) return '';
    let buffer = [],
      objString = btoa(unescape(encodeURIComponent(JSON.stringify(object)))),
      charBuffer = objString.split('');

    charBuffer.forEach((i) => { buffer.push(i.charCodeAt(0)); });

    return new Uint8Array(buffer);
  }

  stringToObject(string) {
    try {
      let rawString = String.fromCharCode.apply(null, string),
      object = JSON.parse(decodeURIComponent(escape(atob(rawString))));

      return object;
    } catch(e) { console.error(e); }

    return null;
  }

	onMessage(packet) {

    try {
      let message = null;
      if(typeof(packet.data) === 'string')
        message = this.stringToObject(packet.data);
      else if (packet.flags.binary || packet.data) //I don't know if those flags are actually set? The standard claims it does.
        mesasge = this.onParseArrayBuffer(packet.data);
      else
        return this.onUnknownMessage(packet);

      if(!listener || !listener.namespace) { console.warn(listener); throw new Exception(`Error: Unknown message`); }
  		let listener = this.state.message_listeners[listener.namespace];

  		listener.emit(message);
    }
    catch(e) { console.error(e); }

	}

  //TODO: Handle this somehow?
  onError(error) {
    console.error(error);
  }

  //TODO: This needs to do more... but whut?
  onClose(message) {
    console.log(`Socket closed for [${message.reason}] #${message.code}`);
  }

  onParseArrayBuffer(data) {
    let buffer = new Uint8Array(data),
      bytes = [];

    buffer.forEach((item) => { bytes.push(item); });
    return this.stringToObject(bytes);
  }

  onUnknownMessage(packet) {
    console.warn('Unknown message detected.');
    console.log(packet);
  }

}


export default WebSocketEntity;

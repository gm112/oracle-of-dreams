var path = require("path");
var webpack = require("webpack");
var fs = require("fs");
//Taken from: https://github.com/webpack/webpack-with-common-libs/blob/master/webpack.config.js

let getSrcFiles = (dirs) => {

	let isDir = (dir, url) => {
		return fs.statSync(path.join(dir, url)).isDirectory();
	},
	readDir = (url, parent_path) => {
		let resolved_path = path.join((parent_path || ''), url);
		fs.readdirSync(resolved_path).forEach((file) => {
			if(!isDir(resolved_path, file))
				result[resolved_path + file.replace(/[^a-z0-9]/gi,'')] = path.resolve(resolved_path, file);
		});
	},
	result = {};

	for(let i in dirs) {
		let dir = dirs[i];
		if(!dir) continue;

		readDir(dir);
	}

	console.log(result);
	return result;
};


module.exports = {
	cache: true,
	context: __dirname,
	entry: './game', //getSrcFiles(['./engine', './game']),
	output: {
		path: path.join(__dirname, "./bin"),
		publicPath: "bin/",
		filename: "[name].js",
		chunkFilename: "[chunkhash].js"
	},
	resolve: {
		alias: {
		'blue-gps-engine': path.join(__dirname, "engine"),
		'blue-gps-game': path.join(__dirname, "game"),
		'static': path.join(__dirname, "static"),
		'phaser': path.join(__dirname, "phaser"),
	},
		modulesDirectories: ['node_modules'],
		extensions: ['', '.js', '.jsx', '.json'],
  },
	module: {
		loaders: [{
             test: /\.js$/,//test: /(pixi|phaser).js|\.js$/,
             exclude: /(node_modules|bower_components)/,
             loader: 'babel',
						 query: {
							 presets: ['latest']
						 }
					 },
					 	{ test: /\.css$/,    loader: "style-loader!css-loader" },
					 	{ test: /\.wav|mp3$/,loader: 'file-loader'  },
						{ test: /\.json$/,loader: 'json-loader'  },
						{ test: /(pixi|phaser).js/, loader: "script" },
						/*{ test: /pixi|phaser.js/, include: [path.join('node_modules', 'phaser'), path.join('')], loader: 'script' }*/
					],

		/*
		loaders: [
			{
				loader: 'script',// script-loader
				test: /(pixi|phaser).js/
			},
			{
        test: /\.json$/,
				include: [
					path.resolve(__dirname, "../static")
				],
        loader: 'json'
			},
			// required to write "require('./style.css')"
			{ test: /\.js?$/,
				exclude: /(node_modules|bower_components)/,
				include: [
	        path.resolve(__dirname, "../engine"),
					path.resolve(__dirname, "../game")
	        //path.resolve(__dirname, "../src/test")
	      ],
	      loader: 'babel', // 'babel-loader' is also a legal name to reference
	      query: {
	        presets: ['es2015']
	      }
			},


			{ test: /\.css$/,    loader: "style-loader!css-loader" },
			{ test: /\.(wav|mp3)$/,loader: 'file-loader'  },

		]*/

	},
	plugins: [
		new webpack.DefinePlugin({
			BUILD_MODE: 'debug'
		})
	]
};
